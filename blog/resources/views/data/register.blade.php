<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
       a:link
       {
           text-decoration:none;
        }
    </style>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
  
        <label for="">First name :</label> <br><br>
        <input type="text">
        <br><br>

        <label for="">Last name :</label> <br><br>
        <input type="text"><br><br>

        <label for="">Gender :</label><br><br>
        <input type="radio" name="Gender"> Male <br>
        <input type="radio" name="Gender"> Female <br>
        <input type="radio" name="Gender"> Other <br><br>

        <label for="">Nationality :</label><br><br>
        <select>
            <option>Indonesian</option>
            <option>Malaysian</option>
            <option>Singapore</option>
            <option>Australian</option>
        </select><br><br>

        <label for="">Language Spoken :</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other<br><br>

        <label for="">Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>

        <button type="submit" value="submit"><a href="/welcome">Sign-Up</a></button>

    </form>

</body>

</html>